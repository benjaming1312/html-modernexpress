$(document).ready(function(){
    $('.banner .row .owl-carousel').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        items:1,
        dots:true,
        animateOut: 'fadeOut',
        autoplay:true,
        autoplayHoverPause:false
    })
    if ($(window).width() < 768) {
        $('.banner').before($('.navbar.navbar-default'));

    }
  })